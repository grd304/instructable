## INCLASS Assignment

* Clone the files from repo.
* Create directory structure

instructables
|- img
|-index.html





https://youtu.be/2v-N7TR1_og

<!--TITLE -->
3D Printing + Cosplay
A perfect combination for making great things! This giant helmet is 3d printed in transparent/translucent PLA for a light up wearable, giving you freedom to headbang to your favorite beats. 

As a hollowed out shell, you can easily to fit a strip of LEDs inside the helmet for making an epic LED costume. You can even customize the helmet in CAD software to fit your head! 


<!--Lists -->

light_hero_still.jpg
Prerequisite Guides
Introducing GEMMA
Introducing Trinket
Introducting Pro Trinket
NeoPixel Uberguide



light_parts2.jpg
<!--Title -->
Parts
We have two Trinket (or Pro Trinket) setups - one for front and one for sides, so you'll need to have two 'kits'!
<!--Lists -->
2 x Trinket 5V - Our demo uses Trinket, but for more complex animations you may want...
or 2 x Pro Trinket 5V - more code space and pins than a Trinket
NeoPixel Strip - more LEDs mean more complex animations but more power draw!
2 x Slide Switch
2 x JST Extension/Connector
2 x 1200mAh Lithium Polymer Battery - bigger batteries last longer but take up more space.
Micro Lipo charger for recharging the batteries
<!--TITLE -->
Tools & Supplies

<!--LIST -->
Solder Iron + Solder
Silicone Wire
PLA Filament
3D Printer
Heat shrink tubing
light_hero-look-lp1.jpg
light_hero_arms_cross.jpg
3D Printing by Ruiz Brothers

<!--Title -->
PLA Filament
Optimized for printing in PLA material, due to the large build. PLA is more dimensionally stable than ABS when it comes to big prints. The helmet is shelled out and prints best with no support or raft material.

<!--Title -->
Download Halmet
light_28.jpg

<!--Title -->
Customize Helmet
Fit the helmet to your head! Measure around the top part of your head, right above the ears and edit the dimensions inside the DP-helment.123dx file, keep the maximum build size in mind. You can even launch 123D Design right in your browser.

<!--Title -->
Launch 123D Design
Before you start printing, make sure that you have enough filament to complete the build! You'll need 1.2 pounds of PLA to finish this giant helmet, so it's a good idea to weigh how much filament is on your spool.

<!--Title -->
Large Build
This jumbo helmet measures a massive 246mm x 226mm x 250mm and takes about 49 hours (3 Days) to print! 
Fuse Filament
If you notice your spool running low, you can heat up two filament ends together with a heating element like a nozzle from a 3D Printing Pen to fuse the ends.

<!--Title -->
Flexy Plate
Easy remove large print off the platform by using a flexable build plate.

<!--Title -->
Slicer Settings
For the best quality when printing with PLA, we recommend the following slice settings:

<!--List -->
Retraction Speed: 800mm
Retraction Distance: 1.5mm
Speeds: 60/80
Shells: 2
Extruder Temp: 225c
Infill 10%
Support: Off
No Heated Bed

Circuit Diagram by Ruiz Brothers
For our demo we're keeping it simple with two low cost Trinkets + neopixel strips that share the same data pin. However, this is just to get you started! Customize your own helmet with your own designs. You can also upgrade to a Pro Trinket which has way more flash space for more complex designs.

light_daft-circuit.jpg
Follow the illustration above as a reference for wiring the components. The trinket micro-controller is wired to the neopixel strip using a Y connection. Each strip is sharing a single connection to the trinket's data, power and ground pins. 
This switch is only for about ~500mA of current (so only maybe 10 or 20 LEDs on at once). If you want to have way more LEDs and higher draw, please use this switch which is rated for 2 Amps
Be sure to wire the NeoPixels to the correct end! Remember it's the one with the arrow pointing to the right.

light_jst-back.jpg
<!--Title -->
TRINKET + NeoPixel Strip
In this circuit diagram, you will need to solder a JST female connector to the bottom of the Trinket where the postive+ and negative- terminals are exposed.

The NeoPixel Strip IN pin is wired to D0 on Trinket. Postive+ pin on NeoPixel Strip is wired to BAT pin on Trinket. The Negative- pin on the strip is wired to GND pin on Trinket.
light_slideswitch.jpg
Slide Switch Adapter
Shorten a JST extension cable to about 10mm long by cutting the positive and negative cables with wire cutters. Use wire stripers to strip the ends of the positive and negative wires. Apply a bit of rosin to the stripped ends and tin the tips of the wires. Add a piece of shrink tubing to the negative wire and solder them together by holding them in place with a third-helping-hand.
Code by Ruiz Brothers
Make sure to to download the NeoPixel Arduino library. Below is front and side code that will change the color of the NeoPixel strip - copy it into your Adafruit Arduino IDE as-is and then mod the LED Pins and number of pixels to make it your own. Remember that to program Trinket you need to download the special Adafruit version of the Arduino IDE from the Introduction to Trinket guide.